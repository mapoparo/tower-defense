﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Node : MonoBehaviour
{
    public Color hoverColor;
    public Color notEnoughMoneyColor;
    private Renderer rend;
    private Color startColor;
    public GameObject turret;
    private BuildManager buildManager;
    public TurretBlueprint turretBlueprint;
    public bool isUpgraded = false;
    private Vector3 posOffset = new Vector3(0, 0.65f, 0);

    void Start()
    {
        rend = GetComponent<Renderer>();
        startColor = rend.material.color;
        buildManager = GameObject.Find("GameManager").GetComponent<BuildManager>();
    }

    public Vector3 GetBuildPosition()
    {
        return transform.position + posOffset;
    }

    void OnMouseEnter()
    {
        if (EventSystem.current.IsPointerOverGameObject())
        {
            return;
        }
        if (!buildManager.CanBuild)
        {
            return;
        }
        if (buildManager.HasMoney)
        {
            rend.material.color = hoverColor;
        }
        else
        {
            rend.material.color = notEnoughMoneyColor;
        }
    }

    void OnMouseExit()
    {
        rend.material.color = startColor;
    }

    void OnMouseDown()
    {
        if (EventSystem.current.IsPointerOverGameObject())
        {
            return;
        }
        if (turret != null)
        {
            buildManager.SelectNode(this);
            return;
        }
        if (!buildManager.CanBuild)
        {
            return;
        }
        else
        {
            BuildTurret(buildManager.GetTurretToBuid());
        }
    }

    void BuildTurret(TurretBlueprint blueprint)
    {
        if (PlayerStats.Money < blueprint.cost)
        {
            Debug.Log("Insufficient funds!");
            return;
        }

        PlayerStats.Money -= blueprint.cost;

        GameObject _turret = (GameObject)Instantiate(blueprint.prefab, GetBuildPosition(), Quaternion.identity);
        turret = _turret;

        turretBlueprint = blueprint;

        if (blueprint.prefab == buildManager.mageTower || blueprint.prefab == buildManager.cannonTower || blueprint.prefab == buildManager.explosiveTower)
        {
            GameObject effect = (GameObject)Instantiate(buildManager.towerBuildEffect, GetBuildPosition(), Quaternion.identity);
            Destroy(effect, 5f);
        }
        else
        {
            GameObject effect = (GameObject)Instantiate(buildManager.buildEffect, GetBuildPosition(), Quaternion.identity);
            Destroy(effect, 5f);
        }

        Debug.Log("Turret build!");
    }

    public void UpgradeTurret()
    {
        if (PlayerStats.Money < turretBlueprint.upgradeCost)
        {
            Debug.Log("Insufficient funds!");
            return;
        }

        PlayerStats.Money -= turretBlueprint.upgradeCost;

        Destroy(turret);

        GameObject _turret = (GameObject)Instantiate(turretBlueprint.upgradedPrefab, GetBuildPosition(), Quaternion.identity);
        turret = _turret;

        if (turretBlueprint.upgradedPrefab == buildManager.upgradedMageTower || turretBlueprint.upgradedPrefab == buildManager.upgradedCannonTower || turretBlueprint.upgradedPrefab == buildManager.upgradedExplosiveTower)
        {
            GameObject effect = (GameObject)Instantiate(buildManager.towerBuildEffect, GetBuildPosition(), Quaternion.identity);
            Destroy(effect, 5f);
        }
        else
        {
            GameObject effect = (GameObject)Instantiate(buildManager.buildEffect, GetBuildPosition(), Quaternion.identity);
            Destroy(effect, 5f);
        }

        isUpgraded = true;

        Debug.Log("Turret upgraded!");
    }

    public void SellTurret()
    {
        PlayerStats.Money += turretBlueprint.GetSellCost();

        GameObject effect = (GameObject)Instantiate(buildManager.sellEffect, GetBuildPosition(), Quaternion.identity);
        Destroy(effect, 5f);

        Destroy(turret);
        turretBlueprint = null;
    }
}