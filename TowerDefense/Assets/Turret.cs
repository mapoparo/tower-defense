﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Turret : MonoBehaviour
{
    public Transform Target;
    private Enemy targetEnemy;
    public Transform bulletSpawnPoint;
    public GameObject bulletPrefab;
    private string[] tags = { "Enemy", "Enemy1", "Enemy2" };
    private Animator a;

    public float range = 2f;
    public Transform partToRotate;
    public float turnSpeed = 10f;
    public float fireRate = 1f;
    private float fireCountDown = 0f;

    [Header("Laser stuff")]
    public bool useLaser = false;
    public LineRenderer lineRenderer;
    public ParticleSystem impactEffect;
    public Light impactLight;
    public float slowPercentage = .5f;

    void Start()
    {
        InvokeRepeating("UpdateTarget", 0, 0.1f);
    }

    void Awake()
    {
        if(transform.childCount > 1 && transform.GetChild(1).transform.childCount > 1)
        a = transform.GetChild(1).transform.GetChild(1).GetComponent<Animator>();
    }

    /// <summary>
    /// Tikrina kuris priesas yra artimiausias ir, jei yra jo range diapazone, 
    /// targetina ji.
    /// </summary>
    void UpdateTarget()
    {
        List<GameObject> enemies = new List<GameObject>();

        foreach (var tag in tags)
        {
            enemies.AddRange(GameObject.FindGameObjectsWithTag(tag));
        }

        float shortestDist = Mathf.Infinity;
        GameObject nearestEnemy = null;

        if (enemies == null)
            return;

        foreach (var enemy in enemies)
        {
            float distanceToEnemy = Vector3.Distance(transform.position, enemy.transform.position);

            if(distanceToEnemy < shortestDist)
            {
                shortestDist = distanceToEnemy;
                nearestEnemy = enemy;
            }
        }

        if(nearestEnemy != null && shortestDist <= range)
        {
            Target = nearestEnemy.transform;
            targetEnemy = nearestEnemy.GetComponent<Enemy>();
        }
        else
        {
            Target = null;
        }
    }

    void Update()
    {
        if (a != null)
            if (a.GetBool("Shoot") == true)
                a.SetBool("Shoot", false);

        if (Target == null)
        {
            if (useLaser)
            {
                if (lineRenderer.enabled)
                {
                    lineRenderer.enabled = false;
                    impactEffect.Stop();
                    impactLight.enabled = false;
                }
            }
            return;
        }

        LockOnTarger();

        if(useLaser)
        {
            Laser();
        }
        else
        {
            if (fireCountDown <= 0)
            {
                Shoot();
                fireCountDown = 1f / fireRate;
            }

            fireCountDown -= Time.deltaTime;
        }
    }

    void Laser()
    {
        targetEnemy.Slow(slowPercentage);

        if (!lineRenderer.enabled)
        {
            lineRenderer.enabled = true;
            impactEffect.Play();
            impactLight.enabled = true;
        }

        lineRenderer.SetPosition(0, bulletSpawnPoint.position);
        lineRenderer.SetPosition(1, Target.position);

        Vector3 dir = bulletSpawnPoint.position - Target.position;

        impactEffect.transform.position = Target.position + dir.normalized * .3f;

        impactEffect.transform.rotation = Quaternion.LookRotation(dir);
    }

    void LockOnTarger()
    {
        Vector3 dir = Target.position - transform.position;
        Quaternion lookRotation = Quaternion.LookRotation(dir);
        Vector3 rotation = Quaternion.Lerp(partToRotate.rotation, lookRotation, Time.deltaTime * turnSpeed).eulerAngles;
        partToRotate.rotation = Quaternion.Euler(0f, rotation.y, 0f);
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, range);
    }

    void Shoot()
    {
        Vector3 dir = Target.position - transform.position;
        //Quaternion lookRot = Quaternion.LookRotation(dir);

        //Vector3 euler = lookRot.eulerAngles;

        var angle = Vector3.Angle(Vector3.left, dir);
        var dot = Vector3.Dot(Vector3.forward, dir);
        angle = dot < 0 ? -angle : angle;

        GameObject bulletGO = Instantiate(bulletPrefab, bulletSpawnPoint.position, Quaternion.identity);
        GameObject gm = bulletGO.transform.GetChild(0).gameObject;

        if(a != null)
            a.SetBool("Shoot", true);

        if (gm != null && gm.name == "rotation")
            gm.transform.rotation = Quaternion.Euler(0, angle, 0);

        Bullet bullet = bulletGO.GetComponent<Bullet>();

        if(bullet != null)
        {
            bullet.Seek(Target);
        }
    }
}
