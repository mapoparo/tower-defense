﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStats : MonoBehaviour
{
    public static int Money;
    public int increase = 5;
    public int startMoney = 400;

    private void Start()
    {
        Money = startMoney;
    }

    public void IncreaseMoney()
    {
        Money += increase;
    }
}
