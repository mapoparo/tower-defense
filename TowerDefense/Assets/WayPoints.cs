﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WayPoints : MonoBehaviour
{
    public static Transform[] points;
    public static Transform[] points1;
    public static Transform[] points2;

    void Awake()
    {
        if (SceneManager.GetActiveScene().name == "ThirdLevel" || SceneManager.GetActiveScene().name == "FourthLevel"
            || SceneManager.GetActiveScene().name == "LastLevel")
        {
            points = new Transform[transform.GetChild(0).childCount];

            for (int i = 0; i < points.Length; i++)
            {
                points[i] = transform.GetChild(0).GetChild(i);
            }

            points1 = new Transform[transform.GetChild(1).childCount];

            for (int i = 0; i < points1.Length; i++)
            {
                points1[i] = transform.GetChild(1).GetChild(i);
            }

            points2 = new Transform[transform.GetChild(2).childCount];

            for (int i = 0; i < points1.Length; i++)
            {
                points2[i] = transform.GetChild(2).GetChild(i);
            }
        }
        else
        {
            points = new Transform[transform.childCount];

            for (int i = 0; i < points.Length; i++)
            {
                points[i] = transform.GetChild(i);
            }
        }
    }

    public static Transform getWaypoints(int pointNumber, int index)
    {
        if (pointNumber == 2)
            return points2[index];
        else if (pointNumber == 1)
            return points1[index];
        else
            return points[index];
    }
}
