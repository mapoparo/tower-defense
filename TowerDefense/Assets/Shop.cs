﻿using UnityEngine;

public class Shop : MonoBehaviour
{
    public TurretBlueprint standartTurret;
    public TurretBlueprint anotherTurret;
    public TurretBlueprint mageTower;
    public TurretBlueprint cannonTower;
    public TurretBlueprint explosiveTower;
    public TurretBlueprint archerTower;


    private BuildManager buildManager;

    private void Start()
    {
        buildManager = BuildManager.instance;
    }

    public void SelectStandartTurret()
    {
        Debug.Log("Standart turret selected.");
        buildManager.SelectTurretToBuild(standartTurret);
    }

    public void SelectAnotherTurret()
    {
        Debug.Log("Another turret selected.");
        buildManager.SelectTurretToBuild(anotherTurret);
    }

    public void SelectMageTower()
    {
        Debug.Log("Mage tower selected.");
        buildManager.SelectTurretToBuild(mageTower);
    }

    public void SelectCannonTower()
    {
        Debug.Log("Cannon tower selected.");
        buildManager.SelectTurretToBuild(cannonTower);
    }

    public void SelectExplosiveTower()
    {
        Debug.Log("Explosive cannon tower selected.");
        buildManager.SelectTurretToBuild(explosiveTower);
    }

    public void SelectArcherTower()
    {
        Debug.Log("Archer tower selected.");
        buildManager.SelectTurretToBuild(archerTower);
    }
}
