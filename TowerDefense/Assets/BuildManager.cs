﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Sitas scriptas laikys visus turretus.

public class BuildManager : MonoBehaviour
{
    public static BuildManager instance;

    private void Awake()
    {
        if (instance != null)
        {
            Debug.LogError("More than one BuildManager in scene!");
            return;
        }
        instance = this;
    }

    public GameObject buildEffect;
    public GameObject towerBuildEffect;
    public GameObject sellEffect;

    public GameObject standartTurretPrefab;
    public GameObject anotherTurretPrefab;
    public GameObject mageTower;
    public GameObject upgradedMageTower;
    public GameObject cannonTower;
    public GameObject upgradedCannonTower;
    public GameObject explosiveTower;
    public GameObject upgradedExplosiveTower;

    private TurretBlueprint turretToBuild;
    private Node selectedNode;

    public NodeUI nodeUI;

    public bool CanBuild { get { return turretToBuild != null; } }

    public bool HasMoney { get { return PlayerStats.Money >= turretToBuild.cost; } }

    public void SelectTurretToBuild(TurretBlueprint turret)
    {
        turretToBuild = turret;

        DeselectNode();
    }

    public void SelectNode(Node node)
    {
        if (selectedNode == node)
        {
            DeselectNode();
            return;
        }

        selectedNode = node;
        turretToBuild = null;

        nodeUI.SetTarget(node);
    }

    public void DeselectNode()
    {
        selectedNode = null;
        nodeUI.Hide();
    }

    public TurretBlueprint GetTurretToBuid()
    {
        return turretToBuild;
    }
}
