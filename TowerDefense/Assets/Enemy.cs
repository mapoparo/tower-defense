﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Enemy : MonoBehaviour
{
    public float startSpeed = 10f;

    public float speed;
    private Transform target;
    private int wavePointIndex = 0;
    private float turnSpeed = 2;
    public int health = 1;
    public GameObject healthPrefab;
    private List<GameObject> healthObjects;
    private int lastDestroyed;
    public EnemySpawner spawner;
    public float offsetY;
    public AudioClip dead;
    public float spawnOffsetY;
    private Vector3 spawnOffset;

    private int enemyNumber;

    public void AddEnemyNumber(int number)
    {
        enemyNumber = number;
    }

    void Awake()
    {
        spawnOffset = new Vector3(0, spawnOffsetY, 0);
    }

    // Start is called before the first frame update
    void Start()
    {
        speed = startSpeed;

        if (SceneManager.GetActiveScene().name == "ThirdLevel" || SceneManager.GetActiveScene().name == "FourthLevel"
            || SceneManager.GetActiveScene().name == "LastLevel")
        {
            target = WayPoints.getWaypoints(enemyNumber, wavePointIndex);
        }
        else
            target = WayPoints.points[wavePointIndex];

        healthObjects = new List<GameObject>();
        float offSet = 0.2f;

        for (int i = 0; i < health; i++)
        {
            GameObject h;

            if (i == 0)
            {
                h = Instantiate(healthPrefab, transform.position + new Vector3(0, offsetY, 0), Quaternion.identity);
                h.transform.parent = transform;
                healthObjects.Add(h);
                continue;
            }
            else
            {
                h = Instantiate(healthPrefab, transform.position + new Vector3(offSet, offsetY, 0), Quaternion.identity);
                h.transform.parent = transform;
            }

            if (i > 0 && offSet < 0)
            {
                offSet *= -1;
                offSet += 0.2f;
            }
            else
                offSet *= -1;


            healthObjects.Add(h);

        }

        lastDestroyed = healthObjects.Count - 1;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 dir = target.position + spawnOffset - transform.position;

        Quaternion lookRotation = Quaternion.LookRotation(dir);
        Vector3 rotation = Quaternion.Lerp(transform.rotation, lookRotation, Time.deltaTime * turnSpeed).eulerAngles;
        transform.rotation = Quaternion.Euler(0, rotation.y, 0);

        transform.Translate(dir.normalized * speed * Time.deltaTime, Space.World);

        if(Vector3.Distance(transform.position, target.position + spawnOffset) <= 0.2f)
        {
            GetNextWayPoint();
        }

        speed = startSpeed;
    }

    /// <summary>
    /// Gauti kita ejimo pointa.
    /// </summary>
    private void GetNextWayPoint()
    {
        wavePointIndex++;

        if(wavePointIndex >= WayPoints.points.Length)
        {
            UIManager uiManager = GameObject.Find("GameManager").GetComponent<UIManager>();
            uiManager.SubstractLives();
            Destroy(gameObject);
            return;
        }

        if (SceneManager.GetActiveScene().name == "ThirdLevel" || SceneManager.GetActiveScene().name == "FourthLevel"
            || SceneManager.GetActiveScene().name == "LastLevel")
        {
            target = WayPoints.getWaypoints(enemyNumber, wavePointIndex);
        }
        else
            target = WayPoints.points[wavePointIndex];
    }

    /// <summary>
    ///  Reikalingas perduoti indexa ka tik sukurtam priesui,
    ///  kad jis siektu ne pradinio pointo, o sunaikinto prieso 
    ///  siekiamo pointo.
    /// </summary>
    /// <param name="i"></param>
    public void PassWavePointIndex(int i)
    {
        wavePointIndex = i;
    }

    public int GetWavePointIndex()
    {
        return wavePointIndex;
    }

    public void LowerHealth()
    {
        health--;
        
        if (tag == "Enemy" || tag == "Enemy1" || tag == "Enemy2")
            StartCoroutine(hurt());

        if (health <= 0)
        {
            //garsas turetu groti per mixeri, nes situo variantu nesireguliuoja garsumas
            AudioSource.PlayClipAtPoint(dead, transform.position);
            EnemySpawner enemySpawner = GameObject.Find("GameManager").GetComponent<EnemySpawner>();

            enemySpawner.EnemyDestroyed(gameObject, enemyNumber);
            Destroy(gameObject);

            PlayerStats stats = GameObject.Find("GameManager").GetComponent<PlayerStats>();
            stats.IncreaseMoney();

            return;
        }

        if(healthObjects[lastDestroyed] != null)
        Destroy(healthObjects[lastDestroyed]);
        lastDestroyed--;
    }

    IEnumerator hurt()
    {
        Renderer rend = gameObject.transform.GetChild(1).GetComponent<Renderer>();

        if (gameObject.name == "Ponis(Clone)")
        {
             rend = gameObject.transform.GetChild(0).GetChild(1).GetComponent<Renderer>();
        }

        if (rend == null || rend.material == null)
            yield break;

        rend.material.SetColor("_Color", Color.red);

        yield return new WaitForSeconds(0.2f);

        rend.material.SetColor("_Color", Color.white);

    }

    public void Slow(float percentage)
    {
        speed = startSpeed * (1 - percentage);
    }
}
