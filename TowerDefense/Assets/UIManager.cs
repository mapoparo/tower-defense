﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

public class UIManager : MonoBehaviour
{
    public static UIManager instance;
    public int livesCount = 3;
    public GameObject gameOver;
    public GameObject heart;
    public GameObject heart1;
    public GameObject heart2;
    public GameObject dheart;
    public GameObject dheart1;
    public GameObject dheart2;
    [SerializeField]
    TextMeshProUGUI KillCounter_TMP;
    [HideInInspector]
    public int killCount;
    public AudioSource audioHurt;
    public AudioSource audioDead;

    // Start is called before the first frame update
    void Start()
    {
        heart.SetActive(true);
        heart1.SetActive(true);
        heart2.SetActive(true);
        dheart.SetActive(false);
        dheart1.SetActive(false);
        dheart2.SetActive(false);
    }
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }
    /// <summary>
    /// Update'inti text'a
    /// </summary>
    public void UpdateKillCounterUI()
    {
        KillCounter_TMP.text = killCount.ToString();
    }
    /// <summary>
    /// Atimti gyvybes ir mesti gameOver, jei 
    /// lives skaicius yra 0.
    /// </summary>
    public void SubstractLives()
    {
        if (livesCount > 0)
        {
            audioHurt.Play();
            livesCount--;
            if (livesCount == 2)
            {
                heart2.SetActive(false);
                dheart2.SetActive(true);
            }
            if(livesCount == 1)
            {
                heart1.SetActive(false);
                dheart1.SetActive(true);
            }
        }
        
        if(livesCount == 0 && gameOver.activeSelf == false)
        {
            audioDead.Play();
            heart.SetActive(false);
            dheart.SetActive(true);
            gameOver.SetActive(true);
        }

        EnemySpawner enemySpawner = GameObject.Find("GameManager").GetComponent<EnemySpawner>();
        enemySpawner.enemiesAlive--;
    }

    /// <summary>
    /// Loadinti per naujo scene.
    /// </summary>
    public void Retry()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
