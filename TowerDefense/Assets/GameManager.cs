﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class GameManager : MonoBehaviour
{
    public string nextLevel = "Level02";
    public TextMeshProUGUI level;
    public int nextLevelIndex = 2;
    public GameObject winLevelUI;

    void Start()
    {
        level.text = "LEVEL " + (nextLevelIndex - 1);
    }
    public void WinLevel()
    {
        winLevelUI.SetActive(true);
    }

    public void Continue()
    {
        if (PlayerPrefs.GetInt("levelReached") < nextLevelIndex)
        {
            PlayerPrefs.SetInt("levelReached", nextLevelIndex);
        }
        SceneManager.LoadScene(nextLevel);
    }
}
