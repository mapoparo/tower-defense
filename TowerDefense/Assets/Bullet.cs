﻿using UnityEngine;

public class Bullet : MonoBehaviour
{
    private Transform target;
    public GameObject impactEffect;
    private float speed = 10f;
    public float explosionRadius = 0;

    public void Seek(Transform target)
    {
        this.target = target;
    }

    void Update()
    {
        //Jei target nera, sunaikinti si objekta.
        if (target == null)
        {
            Destroy(gameObject);
            return;
        }

        Vector3 dir = target.position - transform.position;

        float distanceThisFrame = speed * Time.deltaTime;

        //Jeigu atstumas iki target yra mazesnis arba lygus atstumui sio kadro.
        //Kitaip - jeigu pasieke target.
        if (dir.magnitude <= distanceThisFrame)
        {
            Destroy();
            return;
        }

        transform.Translate(dir.normalized * distanceThisFrame);
    }
    /// <summary>
    /// Sunaikinti kulka ir target bei paleisti efektus.
    /// </summary>
    private void Destroy()
    {
        GameObject effectIns = Instantiate(impactEffect, transform.position, Quaternion.identity);
        Destroy(effectIns, 2f);

        if (explosionRadius > 0f)
        {
            Collider[] colliders = Physics.OverlapSphere(transform.position, explosionRadius);
            foreach (Collider collider in colliders)
            {
                if (collider.tag == "Enemy" || collider.tag == "Enemy1" || collider.tag == "Enemy2")
                {
                    collider.transform.GetComponent<Enemy>().LowerHealth();
                }
            }
        }
        else
        {
            target.GetComponent<Enemy>().LowerHealth();
        }
        //EnemySpawner enemySpawner = GameObject.Find("GameManager").GetComponent<EnemySpawner>();

        //enemySpawner.EnemyDestroyed(target.gameObject);
        //Destroy(target.gameObject);
        Destroy(gameObject);
        return;
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawSphere(transform.position, explosionRadius);
    }
}
