﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EnemySpawner : MonoBehaviour
{
    public Transform startPos;
    public Transform startPos1;
    public Transform startPos2;

    public GameObject enemy;
    public GameObject enemy1;
    public GameObject enemy2;
    private int spawnCount = 0;
    private GameObject instEnemy;
    private int waveIndex = 0;
    public int stopWavesIndex;
    public GameManager gameManager;
    public int enemiesAlive = 0;
    public float spawnOffsetY;

    // Start is called before the first frame update
    void Start()
    {
        instEnemy = enemy;
        StartCoroutine(Spawn());
    }


    private void Update()
    {
        UIManager uiManager = GameObject.Find("GameManager").GetComponent<UIManager>();
        if (waveIndex == stopWavesIndex && enemiesAlive == 0 && uiManager.livesCount != 0)
        {
            gameManager.WinLevel();
        }
    }

    /// <summary>
    /// Kas kazkiek laiko spawninti priesus, didinti ju skaiciu ir 
    /// sunkinti.
    /// </summary>
    /// <returns></returns>
    IEnumerator Spawn()
    {
        bool isSpawning = true;
        while (isSpawning)
        {
            waveIndex++;
            if (waveIndex == stopWavesIndex)
            {
                isSpawning = false;
            }
            for (int i = 0; i <= spawnCount; i++)
            {
                enemiesAlive++;

                Vector3 offSet = Vector3.zero;

                if (instEnemy != null && instEnemy.GetComponent<Enemy>() != null)
                    offSet = new Vector3(0, instEnemy.GetComponent<Enemy>().spawnOffsetY, 0);

                if (SceneManager.GetActiveScene().name == "ThirdLevel" || SceneManager.GetActiveScene().name == "FourthLevel"
                    || SceneManager.GetActiveScene().name == "LastLevel")
                {
                    int enemyNumber = Random.Range(0, 3);

                    GameObject enemy;

                    if (enemyNumber == 2)
                        enemy = Instantiate(instEnemy, startPos2.position + offSet, Quaternion.identity);
                    else if(enemyNumber == 1)
                        enemy = Instantiate(instEnemy, startPos1.position + offSet, Quaternion.identity);
                    else
                        enemy = Instantiate(instEnemy, startPos.position + offSet, Quaternion.identity);

                    if (enemy != null)
                        if (enemy.GetComponent<Enemy>() != null)
                            enemy.GetComponent<Enemy>().AddEnemyNumber(enemyNumber);
                }
                else
                    Instantiate(instEnemy, startPos.position + offSet, Quaternion.identity);

                yield return new WaitForSeconds(0.5f);
            }

            spawnCount++;

            if (spawnCount == 2)
                instEnemy = enemy1;
            else if (spawnCount == 4)
                instEnemy = enemy2;

            yield return new WaitForSeconds(2);
        }
    }

    /// <summary>
    /// Koks priesas buvo sunaikintas ir koki nauja vietoje jo sukurti?
    /// </summary>
    /// <param name="target"></param>
    public void EnemyDestroyed(GameObject target, int enemyNumber)
    {
        if(target.tag == "Enemy1" && SceneManager.GetActiveScene().name != "LastLevel")
        {
            Vector3 offSet = Vector3.zero;

            if (enemy != null && enemy.GetComponent<Enemy>() != null)
                offSet = new Vector3(0, enemy.GetComponent<Enemy>().spawnOffsetY, 0);

            GameObject obj = Instantiate(enemy, new Vector3(target.transform.position.x, startPos.transform.position.y + offSet.y, target.transform.position.z), Quaternion.identity);
            obj.GetComponent<Enemy>().PassWavePointIndex(target.GetComponent<Enemy>().GetWavePointIndex());

            if (enemyNumber != -1)
            {
                if (obj != null)
                    if (obj.GetComponent<Enemy>() != null)
                    {
                        obj.GetComponent<Enemy>().AddEnemyNumber(enemyNumber);
                    }
            }
        }
        else
        {
            UIManager.instance.killCount++;
            UIManager.instance.UpdateKillCounterUI();
            enemiesAlive--;
        }
    }
}
